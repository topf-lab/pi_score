import subprocess
from Bio.PDB import *
from Bio import SeqIO
from Bio.PDB.Polypeptide import three_to_one
import os
import re
import sys
from Bio import PDB
import setup_names

def get_ch_seq(pdbFile,outfile,chain_ids=None):
    p = PDBParser(PERMISSIVE=1, QUIET=True)
    structure = p.get_structure(pdbFile, pdbFile)
    chain_lst = chain_ids
#     if chain_ids:
#         chain_lst = ''.join(chain_ids.split())
    for model in structure:
        for chain in model:
            seq = list()
            chainID = str(chain.get_id())
            modelID = str(model.get_id())
            pdbID = str(pdbFile.rsplit('.',1)[0])
            if chain_lst:
                if chainID not in chain_lst:
                    continue
            name = pdbID + '_' + modelID + '_' + chainID
            for residue in chain:
                if residue.id[0] != ' ':
                    continue
                if is_aa(residue.get_resname(), standard=True):
                    seq.append(three_to_one(residue.get_resname()))
                else:
                    seq.append("X")
            out = open(outfile + '/' + name + '_ch.fasta','w')
            out.write(">" + name + "\n" + str("".join(seq)))
            out.close()

def write_all_ch_seq(outfile_name=None,infile_list=None):
    '''
    infile_list is the list containing names of all chain fastas
    '''
    assert infile_list is not None
    assert outfile_name is not None
    with open(outfile_name, 'w') as w_file:
        for filen in infile_list:
            with open(filen, 'rU') as o_file:
                seq_records = SeqIO.parse(o_file, 'fasta')
                SeqIO.write(seq_records, w_file, 'fasta')
                
def run_r4site(cmd):
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    p.wait()
    
def filter_homologs(psiout):
    '''
    check the domain boundaries of homologs that align and trim the sequneces of homologs
    '''
    if not os.path.isfile(psiout):
        sys.exit("Input file not found!")
    #qseqid sseqid pident length qstart qend sstart send evalue qcovs bitscore
    outlist = os.path.splitext(psiout)[0]+"_seq.lst"
    dict_bound = {}
    o = open(outlist,'w')
    f = open(psiout,'r')
    for l in f:
        lsplit = l.split()
        if len(lsplit) < 5: continue
        if int(lsplit[-2]) >= 80:
            o.write(lsplit[1]+"\n")
            dict_bound[lsplit[1]] = [int(lsplit[6]),int(lsplit[7])]
    o.close()
    f.close()
    outfas = os.path.splitext(psiout)[0]+"_seq.fasta"
    outfasta = os.path.splitext(psiout)[0]+"_seqcut.fasta"
    cmd = setup_names.blastdbcmd_path +  " -db " + setup_names.db_path + " -entry_batch %s"%(os.path.abspath(outlist))
    
    with open(os.path.abspath(outfas),'w') as ou:
        subprocess.call(cmd.split(),stdout=ou)
    if not os.path.isfile(outfas):
        sys.exit("Seq fasta file not found!")
    ow = open(outfasta,'w')
    o = open(outfas,'r')
    fl = 0
    s, e = 0,0
    sequ, gid = '', ''
    for l in o:
        if l[0] == '>':
            if fl == 1:
                ow.write(">"+gid+"\n")
                ow.write(sequ[s-1:e]+"\n")
            fl = 0
            for k in dict_bound:
                gid = k.split('|')[1]
                if re.search(re.escape(gid),l) != None:
                    s,e = dict_bound[k][0],dict_bound[k][1]
                    fl = 1
                    sequ = ''
                    break
        else:
            if fl == 1:
                sequ += l[:-1]
    ow.write(">"+gid+"\n")
    ow.write(sequ[s-1:e]+"\n")
    ow.close()
    o.close()

def split_bin(a, n):
    '''
    Given a sorted list a it will split it into almost n equal bins
    '''
    k, m = divmod(len(a), n)
    return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in xrange(n))

def get_pdb_numbering(pdbfile):
    '''
    for a given pdbfile returns a dictionry with chain ID as key and list of residue numbers
    '''
    p = PDB.PDBParser(QUIET=True)
    structure = p.get_structure('id',pdbfile)
    ch_residues = {}
    for model in structure:
        for chain in model:
            for residue in chain:
                if residue.id[0] != ' ': continue
                ch = residue.get_full_id()[2]
                res_num = residue.get_full_id()[3][1]
                try:
                    ch_residues[chain.get_id()].append(res_num)
                except KeyError:
                    ch_residues[chain.get_id()] = [res_num]
    return ch_residues

